# Introduction

ALEX is a ML-enhanced range index, similar in functionality to a B+ Tree.
My implementation is based on the origianl ALEX work from https://github.com/microsoft/ALEX.

# Main Contributions

Built up from the previous researchers, in this repository, I have also added the following
1. Simple YCSB benchmarks testing for workload A and B
2. Concurrency control for write-heavy (reads and updates) workload
3. Concurrency control for insert-heavy (reads and inserts) workload
4. Quadratic regression model to fit the data distribution

# Getting Started
ALEX can be used as a header-only library.
All relevant header files are found in [src/core](src/core).
You will need to compile your program with at least the C++14 standard (e.g., `-std=c++14`).

On Windows, simply load this repository into Visual Studio as a CMake project.
On Linux/Mac, use the following commands:
```
# Build using CMake, which creates a new build directory
./build.sh

# Run example program
./build/example

# Run unit tests
./build/test_alex
```
If you happen to run into any Windows line-endings issues on Mac OS. Please use [build_new.sh](build_new.sh) for Mac OS instead of build.sh.
To run the benchmark on a synthetic dataset with 1000 normally-distributed keys:
```
./build/benchmark \
--keys_file=resources/sample_keys.bin \
--keys_file_type=binary \
--init_num_keys=500 \
--total_num_keys=1000 \
--batch_size=1000 \
--insert_frac=0.5
```

However, to observe the true performance of ALEX, we must run on a much larger dataset.
You can download a 200M-key (1.6GB) dataset from [Google Drive](https://drive.google.com/file/d/1zc90sD6Pze8UM_XYDmNjzPLqmKly8jKl/view?usp=sharing).
To run one example workload on this dataset:
```
./build/benchmark \
--benchmarks=ycsbwklda \
--keys_file=[download location] \
--keys_file_type=binary \
--init_num_keys=10000000 \
--total_num_keys=20000000 \
--batch_size=1000000 \
--insert_frac=0.5 \
--print_batch_stats \
--value_size=1000
```

You can also run this benchmark on your own dataset.
Your keys will need to be in either binary format or text format (one key per line).
