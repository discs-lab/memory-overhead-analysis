// Copyright (c) Microsoft Corporation.
// Licensed under the MIT license.

/*
 * Simple benchmark that runs a mixture of point lookups and inserts on ALEX.
 */

#include "../core/alex.h"

#include <iomanip>
#include <thread>
#include <chrono>


#include "flags.h"
#include "utils.h"
#include <ctime>
#include <unistd.h>

//added by XINRAN XIONG
#include <iostream>
#include <random>
using namespace std;

// Modify these if running your own workload
#define KEY_TYPE double
#define PAYLOAD_TYPE double

/*
 * Required flags:
 * --keys_file              path to the file that contains keys
 * --keys_file_type         file type of keys_file (options: binary or text)


 * --init_num_keys          number of keys to bulk load with
 * --total_num_keys         total number of keys in the keys file
 * --batch_size             number of operations (lookup or insert) per batch
 * --benchmarks             type of benchmarks
 *
 * Optional flags:
 * --insert_frac            fraction of operations that are inserts (instead of
 * lookups)
 * --lookup_distribution    lookup keys distribution (options: uniform or zipf)
 * --time_limit             time limit, in minutes
 * --print_batch_stats      whether to output stats for each batch
 */

// added by XINRAN XIONG
//  return a radom double generator


double randomDoubleGenerator (double min, double max)
{
      static std::default_random_engine generator;
      std::uniform_real_distribution<double> distribution (min,max);
      return distribution(generator);
}

// memory test benchmark
int default_benchmarks(
                       int init_num_keys, int total_num_keys, int batch_size, double insert_frac,
                       bool print_batch_stats);


int main(int argc, char *argv[]) {

    
    
    // get the pid of the running program
    int pid = getpid();
    std::cout << "pid of this program is: " << pid << std::endl;;

    //sleep 10 sec and then start running
    std::this_thread::sleep_for(std::chrono::milliseconds(10000));

    auto flags = parse_flags(argc, argv);
    auto init_num_keys = stoi(get_required(flags, "init_num_keys"));
    auto total_num_keys = stoi(get_required(flags, "total_num_keys"));
    auto batch_size = stoi(get_required(flags, "batch_size"));
    auto insert_frac = stod(get_with_default(flags, "insert_frac", "0.5"));
    bool print_batch_stats = get_boolean_flag(flags, "print_batch_stats");


    default_benchmarks(init_num_keys,total_num_keys, batch_size,
        insert_frac, print_batch_stats);
}


int default_benchmarks(
                       int init_num_keys, int total_num_keys, int batch_size, double insert_frac,
                       bool print_batch_stats) {
    // Read keys from a random generator 
    // random number between 0.0 and 1.0
    static std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution (0,1);

    // Combine bulk loaded keys with randomly generated payloads
    auto values = new std::pair<KEY_TYPE, PAYLOAD_TYPE>[init_num_keys];
    std::mt19937_64 gen_payload(std::random_device{}());
    for (int i = 0; i < init_num_keys; i++) {
        values[i].first = distribution(generator);
        values[i].second = distribution(generator);
    }

    // Create ALEX and bulk load
    alex::Alex<KEY_TYPE, PAYLOAD_TYPE> index;
    std::sort(values, values + init_num_keys,
              [](auto const &a, auto const &b) { return a.first < b.first; });
    index.bulk_load(values, init_num_keys);

    //sleep 10 sec after bulk load
    std::cout << "sleep 10 sec after bulk load" << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(10000));

    //remove the bulk load array
    delete[] values;

    // Run workload
    int i = init_num_keys;  //  number of bulk load keys
    long long cumulative_inserts = 0;   //  number of performed inserts
    long long cumulative_lookups = 0;   // number of performed lookups
    int num_inserts_per_batch = static_cast<int>(batch_size * insert_frac); //  inserts per batch(loop)
    int num_lookups_per_batch = batch_size - num_inserts_per_batch; //  looksups per batch(loop)
    
    std::cout << "batch_size: " << batch_size << "   " 
              << "insert_fraction: " << insert_frac << "   " 
              << "read_per_batch: " << num_lookups_per_batch << "   " 
              << "write_per_batch: " << num_inserts_per_batch << std::endl;;

    int batch_no = 0;
    
    while (true) {
        batch_no++;

        // get insert num
        int num_actual_inserts =
                std::min(num_inserts_per_batch, total_num_keys - i);
        if (num_actual_inserts < num_inserts_per_batch)
        {
            // redefine number of reads in last batch
            num_lookups_per_batch = (num_actual_inserts / insert_frac) * (1.0 - insert_frac);
        }
        //  Do readings
        for (int j = 0; j < num_lookups_per_batch; j++) {
            KEY_TYPE key = distribution(generator);
            index.get_payload(key);
        }
        
        cumulative_lookups += num_lookups_per_batch;

        // Do inserts
        num_actual_inserts =
                std::min(num_inserts_per_batch, total_num_keys - i);
        int num_keys_after_batch = i + num_actual_inserts;
        for (; i < num_keys_after_batch; i++) {
            KEY_TYPE insert_key = distribution(generator);
            index.insert(insert_key,distribution(generator));
        }
        
        cumulative_inserts += num_actual_inserts;

        if (print_batch_stats) {
            long long cumulative_operations = cumulative_lookups + cumulative_inserts;
            std::cout << "Batch: " << batch_no
                      << ", cumulative ops: " << cumulative_operations
                      << "\n\tbatch throughput: \t"
                      << "num_lookups_per_batch: "
                      << num_lookups_per_batch << "   "
                      << "num_inserts_per_batch: "
                      << num_actual_inserts << "   "
                      << " total number of insertion done: "
                      << cumulative_inserts << "   "
                      << "total number of reading done: "
                      << cumulative_lookups
                      << std::endl;
        }

        // Check for workload end conditions
        if (num_actual_inserts < num_inserts_per_batch) {
            // End if we have inserted all keys in a workload with inserts
            std::cout << "end because we have inserted all keys in a workload with inserts"
                      << std::endl;
            break;
        }

    }
    
    // after all keys are inserted into ALEX, we run the program for in place updaing and reading
    //sleep 10 sec and then start running
    std::cout << "sleep for 10 seconds"
                      << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(10000));
    std::cout << "start reading and updating"
                      << std::endl;
    int updates_num = 250000000;    //  number of updates to perform
    int batch_no_after = 0; //  count the batch number
    int real_cumulative_read = 0;   //  number of reads in total
    int real_cumulative_update = 0; //  number of updates in total

    while(true)
    {
      
        batch_no_after++;

        //  get the number of updates per batch
        int num_actual_updates =
                std::min(num_inserts_per_batch, static_cast<int>(updates_num - real_cumulative_update));
        
        if (num_actual_updates < num_inserts_per_batch)
        {
            // redefine number of reads in last batch
            num_lookups_per_batch = (num_actual_updates / insert_frac) * (1.0 - insert_frac);
        }
        
        // reading
        for (int j = 0; j < num_lookups_per_batch; j++) {
            KEY_TYPE key = distribution(generator);
            index.get_payload(key);
        }
        real_cumulative_read = real_cumulative_read + num_lookups_per_batch;
        
        //updating
        for (int i = 0; i < num_actual_updates; i++) {
            
                KEY_TYPE update_key = distribution(generator);
                auto it = index.lower_bound(update_key);
                // key exists, update
                if (it != index.end()) {
                    it.payload() = distribution(generator);
                }
                //key does not exist, does not count as an update
                else{
                    i--;
                }
            
        }
        real_cumulative_update = num_actual_updates + real_cumulative_update;

        //  after all the updates are done, the program stops
        if (real_cumulative_update == updates_num)
        {
            std::cout << "read and update complete" << std::endl;
            break;
        }
        
    }

    return 0;
}


