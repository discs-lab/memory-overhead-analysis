# Memory overhead analysis in LSM and Learned Indexes

Author: Xinran Xiong
COMP-400 Project

# Main Contributions
1. For ALEX, memory test benchmark is implemented in file alex_ycsb-master/src/benchmark/main.cpp.
2. For RocksDB, memory test benchmark is implemented in file rocksdb-master/tools/db_bench_tool.cc.

# Getting Started - ALEX
On Mac OS, use the following command to compile our benchmark program:
```
# Start by redirecting to alex_ycsb-master folder
cd alex_ycsb-master
# Build using CMake, which creates a new build directory
./build_new.sh
```
To run the memory test benchmark on a randomly generated dataset with 8000000 keys:
```
./build/benchmark \
--init_num_keys=1000 \
--total_num_keys=8000000 \
--batch_size=1000 \
--insert_frac=1.0 \
--print_batch_stats=true 
```
User-defined options in command:
```
init_num_keys: number of keys for bulk loading
total_num_keys: total number of keys to insert into ALEX
batch_size: batch size
insert_frac: proportion of write per batch
print_batch_stats: whether to print batch information
```

# Getting Started - ROCKSDB
On Mac OS, use the following command to compile our benchmark program:
```
# Start by redirecting to rocksdb-master folder
cd rocksdb-master
# Compile
sudo make clean
sudo make static_lib
sudo make db_bench
```
To run the memory test benchmark on a randomly generated dataset with 8000000 keys:
```
./db_bench \
--benchmarks=memorytestbenchmark \
--compression_type=none --threads=1 \
--key_size=8 \
--value_size=8 \
--compression_ratio=1 \
--num=8000000 \
--write_buffer_size=6434217728 \
--cache_size=0 \
--max_write_buffer_number=1 \
--statistics=1 \
--histogram=1 \
--stats_interval=100000 \
--stats_per_interval=1 \
--bloom_bits=0 \
--inplace_update_support=true \
--batch_size=1000
```
User-defined options in command: You can find all the RocksDB options here: (https://github.com/facebook/rocksdb/wiki/Benchmarking-tools)

# Memory Monitor
The commands that I used to monitor the memory consumption of the experiment programs are htop and top.
After a program starts, it prints its program_pid on screen. 
To monitor the memory consumption of a specific program on Mac, we can run:
```
htop -p program_pid
```
However, the htop output can not be redirected to a text file. In order to track all the data, we can run:
```
top -pid program_pid > memory_consumption.txt
```
to save the top outputs to the file memory_consumption.txt.

You can find the htop man page here (https://man7.org/linux/man-pages/man1/htop.1.html) and top man page here (https://man7.org/linux/man-pages/man1/top.1.html).
